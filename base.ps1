#Start-BitsTransfer -Source "https://bitbucket.org/gjaves/javes/raw/master/install_chocolatey_win.cmd" -Destination "$env:tmp"
#Start-BitsTransfer -Source "https://bitbucket.org/gjaves/javes/raw/master/install_python_win.cmd" -Destination "$env:tmp"
#Start-BitsTransfer -Source "https://bitbucket.org/gjaves/javes/raw/master/teredo.cmd" -Destination "$env:tmp"

#$cmdfile = Join-Path -Path "$env:tmp" -ChildPath "install_chocolatey_win.cmd"
#cmd /c $cmdfile

#$cmdfile = Join-Path -Path "$env:tmp" -ChildPath "install_python_win.cmd"
#cmd /c $cmdfile

#$cmdfile = Join-Path -Path "$env:tmp" -ChildPath "teredo.cmd"
#cmd /c $cmdfile

#electrum tor-browser veracrypt
#powershell-packagemanagement

$Boxstarter.RebootOk=$true # Allow reboots? 

$Boxstarter.NoPassword=$false # Is this a machine with no login password? 

$Boxstarter.AutoLogin=$true # Save my password securely and auto-login after a reboot 

# Basic setup 
Update-ExecutionPolicy Unrestricted 

enable-computerrestore -drive "$env:SystemDrive" 
Checkpoint-Computer -Description "InstallBase" -RestorePointType "MODIFY_SETTINGS"

Enable-RemoteDesktop 
Disable-UAC 

$cacheLocation = Join-Path -Path "$env:ProgramData" -ChildPath "Package Cache"
New-Item -ItemType directory -Path $cacheLocation

Set-BoxstarterConfig -LocalRepo $cacheLocation

#choco config set cacheLocation "$env:TEMP"

choco feature disable -n=stopOnFirstPackageFailure
choco feature disable -n=scriptsCheckLastExitCode
choco feature disable -n=failOnInvalidOrMissingLicense
choco feature disable -n=usePackageExitCodes
choco feature disable -n=failOnAutoUninstaller
choco feature disable -n=failOnStandardError
choco feature disable -n=checksumFiles
choco feature enable -n=useRememberedArgumentsForUpgrades
choco feature enable -n=ignoreInvalidOptionsSwitches
choco feature enable -n=allowEmptyChecksums
choco feature enable -n=allowEmptyChecksumsSecure
choco feature enable -n=autoUninstaller

cinst -y chocolatey
if (Test-PendingReboot) { Invoke-Reboot } 

cinst -y powershell-packagemanagement
cinst -y boxstarter.windowsupdate
cinst -y chocolatey-windowsupdate.extension
cinst -y pswindowsupdate

if (Test-PendingReboot) { Invoke-Reboot } 

cup -y all

if (Test-PendingReboot) { Invoke-Reboot } 

# Update Windows and reboot if necessary 
Install-WindowsUpdate -AcceptEula 

if (Test-PendingReboot) { Invoke-Reboot } 

Install-WindowsUpdate -Criteria "IsHidden=0 and IsInstalled=0 and Type='Software' and BrowseOnly=0" -AcceptEula

if (Test-PendingReboot) { Invoke-Reboot } 

Install-WindowsUpdate -Criteria "IsHidden=0 and IsInstalled=0 and Type='Driver' and BrowseOnly=0" -AcceptEula

if (Test-PendingReboot) { Invoke-Reboot } 
