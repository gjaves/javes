set cacheLocation=%ProgramData%\choco\cache

mkdir %cacheLocation%

%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/gjaves/javes/raw/master/create_restore_point.ps1'))"

cver || %SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"
call refreshenv

choco config set --name cacheLocation --value %cacheLocation% 

choco feature disable -n=stopOnFirstPackageFailure
choco feature disable -n=scriptsCheckLastExitCode
choco feature disable -n=failOnInvalidOrMissingLicense
choco feature disable -n=usePackageExitCodes
choco feature disable -n=failOnAutoUninstaller
choco feature disable -n=failOnStandardError
choco feature disable -n=checksumFiles
choco feature enable -n=useRememberedArgumentsForUpgrades
choco feature enable -n=ignoreInvalidOptionsSwitches
choco feature enable -n=allowEmptyChecksums
choco feature enable -n=allowEmptyChecksumsSecure
choco feature enable -n=autoUninstaller
call refeshenv

cinst -y powershell4
cinst -y PowerShell
cinst -y powershell-packagemanagement
cinst -y boxstarter.windowsupdate
cinst -y chocolatey-windowsupdate.extension
cinst -y pswindowsupdate
call refeshenv

cup -y chocolatey
call refreshenv

