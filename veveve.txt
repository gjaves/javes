cmd /c choco feature disable -n=stopOnFirstPackageFailure
cmd /c choco feature disable -n=scriptsCheckLastExitCode
cmd /c choco feature disable -n=failOnInvalidOrMissingLicense
cmd /c choco feature disable -n=usePackageExitCodes
cmd /c choco feature disable -n=failOnAutoUninstaller
cmd /c choco feature disable -n=failOnStandardError
cmd /c choco feature disable -n=checksumFiles

cmd /c choco feature enable -n=useRememberedArgumentsForUpgradese
cmd /c choco feature enable -n=ignoreInvalidOptionsSwitches
cmd /c choco feature enable -n=allowEmptyChecksums
cmd /c choco feature enable -n=allowEmptyChecksumsSecure
cmd /c choco feature enable -n=autoUninstaller

Disable-InternetExplorerESC
Disable-GameBarTips
Disable-BingSearch

Set-WindowsExplorerOptions -EnableShowFullPathInTitleBar 
Set-WindowsExplorerOptions -EnableOpenFileExplorerToQuickAccess 
Set-WindowsExplorerOptions -EnableShowRecentFilesInQuickAccess 
Set-WindowsExplorerOptions -EnableShowFrequentFoldersInQuickAccess
Set-WindowsExplorerOptions -EnableExpandToOpenFolder
Set-WindowsExplorerOptions -DisableShowHiddenFilesFoldersDrives 
Set-WindowsExplorerOptions -DisableShowProtectedOSFiles 
Set-WindowsExplorerOptions -DisableShowFileExtensions
Set-TaskbarOptions -Size Large
Set-TaskbarOptions -Lock 
Set-TaskbarOptions -Dock Bottom 
Set-TaskbarOptions -Combine Full 
Set-TaskbarOptions -AlwaysShowIconsOff

cmd /c cinst -y bonjour
cmd /c cinst -y 7zip
cmd /c cinst -y clamwin clamsentinel
cmd /c cinst -y libreoffice
cmd /c cinst -y cmder
cmd /c cinst -y vim
cmd /c cinst -y partitionwizard
cmd /c cinst -y freecommander-xe.portable
cmd /c cinst -y GoogleChrome
cmd /c cinst -y googledrive
cmd /c cinst -y flashplayerplugin
cmd /c cinst -y adobereader-update
cmd /c cinst -y vlc
cmd /c cinst -y jdownloader
cmd /c cinst -y imdisk
cmd /c cinst -y tigervnc
cmd /c cinst -y yumi
cmd /c cinst -y openssh -params '"/SSHServerFeature"'
cmd /c cinst -y chocolateygui
cmd /c cinst -y autologon
cmd /c cinst -y homepages-winconfig --params "'/ALL:YES /URL:google.com'"
cmd /c cinst -y bulk-crap-uninstaller
cmd /c cinst -y teamviewer
cmd /c cinst -y teracopy

cmd /c cinst -y powershell-packagemanagement
cmd /c cinst -y boxstarter.windowsupdate
cmd /c cinst -y chocolatey-windowsupdate.extension
cmd /c cinst -y pswindowsupdate

cmd /c cup -y all


