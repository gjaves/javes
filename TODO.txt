cad/cam/arch

cinst -y sweet-home-3d
cinst -y freecad 
cinst -y googleearthpro 
cinst -y vlc 
cinst -y openshot
cinst -y 7zip 
cinst -y zim
cinst -y qalculate

cinst -y mucommander 
cinst -y mc 
cinst -y freecommander-xe
cinst -y vim

cinst -y fusioninventory-agent --installargs "/acceptlicense /add-firewall-exception /execmode=Service /installtasks=Deploy,Inventory,NetDiscovery,NetInventory,WakeOnLan /local=C:\Fusion /no-start-menu /runnow /S /scan-homedirs /tag=offline-collect"

cinst -y openssh -params '"/SSHServerFeature"'

